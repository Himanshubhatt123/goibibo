package com.goibibo.services;

import java.io.BufferedReader;
import java.nio.file.Path;

import javax.annotation.PostConstruct;
import org.springframework.stereotype.Service;
import com.goibibo.helper.Helpers;

@Service
public class LoadCSVService {
	
	@PostConstruct
	public void init() {
		BufferedReader br=null;
		try {
			 br=Helpers.getPath();
		}catch(Exception e) {
			e.printStackTrace();
			return;
		}
		Helpers.readFile(br);
	}
}

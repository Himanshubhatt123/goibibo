FROM openjdk:8
ADD target/Docker_Goibibo.jar Docker_Goibibo.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "Docker_Goibibo.jar"] 
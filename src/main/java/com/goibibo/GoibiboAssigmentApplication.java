package com.goibibo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GoibiboAssigmentApplication {

	public static void main(String[] args)  {
		SpringApplication.run(GoibiboAssigmentApplication.class, args);
	}
}

